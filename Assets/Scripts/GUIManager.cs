﻿using UnityEngine;
using System.Collections;

public enum GUIState
{	
	LoadingPanel, MainMenu, Difficulty, Options, Credits
}

public class GUIManager : Singleton<GUIManager>
{
	private static GUIState guiState;

	public delegate void GUIStateHandler(GUIState guiState);
	public static event GUIStateHandler OnGUIStateChanged;

	public static GUIState GUIState
	{
		get{	return  guiState;		}
		set{ guiState = value;		
				if (OnGUIStateChanged != null)
					OnGUIStateChanged( guiState );
		}
	}

	void Start () {
		GUIManager.GUIState = GUIState.LoadingPanel;
	}
	
}
