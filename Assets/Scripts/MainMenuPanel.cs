using UnityEngine;
using System.Collections;

public class MainMenuPanel : MonoBehaviour 
{
	private dfPanel panel;
	private dfPanel derp;
	void OnEnable()
	{
		panel = GetComponent<dfPanel> ();
		GUIManager.OnGUIStateChanged += OnGUIStateChanged;
	}
	
	void OnDisable()
	{
		GUIManager.OnGUIStateChanged -= OnGUIStateChanged;
	}
	
	void OnGUIStateChanged (GUIState guiState)
	{
		switch (guiState) {
			
		case GUIState.MainMenu:
			panel.IsVisible = (guiState == GUIState.MainMenu);
			break;
			
		default:
			panel.IsVisible = ((guiState == GUIState.Difficulty)|| (guiState == GUIState.Options)||(guiState == GUIState.Credits));
			break;
		}
	}

	public void MainMenu()
	{
		GUIManager.GUIState = GUIState.MainMenu;
	}

	public void NewGame () {
		GUIManager.GUIState = GUIState.Difficulty;
		}

	public void Options () {
		GUIManager.GUIState = GUIState.Options;
	}

	public void Credits () {
		GUIManager.GUIState = GUIState.Credits;
	}

	public void Exit () {
		Debug.Log ("Exits the game");
		//GUIManager.GUIState = GUIState.Options;
	}
}
