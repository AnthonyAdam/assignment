﻿using UnityEngine;
using System.Collections;

public class Spinner : MonoBehaviour {

	public float rotateSpeed = 120;
	
	void Update () 
	{
		transform.Rotate(Vector3.back * rotateSpeed * Time.deltaTime);                 
	}
}
