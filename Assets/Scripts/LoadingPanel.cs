﻿using UnityEngine;
using System.Collections;

public class LoadingPanel : MonoBehaviour {
	
	private dfPanel panel;
	
	void OnEnable()
	{
		panel = GetComponent<dfPanel> ();
		GUIManager.OnGUIStateChanged += OnGUIStateChanged;
	}

	void OnDisable()
	{
		GUIManager.OnGUIStateChanged -= OnGUIStateChanged;
	}

	void OnGUIStateChanged (GUIState guiState)
	{
		switch (guiState) {
		
		case GUIState.LoadingPanel:
			panel.IsVisible = true;
			break;

		default:
			panel.IsVisible = false;
			break;
		}
	}

	void Start()
	{
		StartCoroutine(Visibility());
	}

	public IEnumerator Visibility()
	{
		yield return new WaitForSeconds(3.0f);
		GUIManager.GUIState = GUIState.MainMenu;
	}
}


























