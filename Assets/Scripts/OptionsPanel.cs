using UnityEngine;
using System.Collections;

public class OptionsPanel : MonoBehaviour {

	private dfPanel panel;

	void OnEnable()
	{
		panel = GetComponent<dfPanel> ();
		GUIManager.OnGUIStateChanged += OnGUIStateChanged;
	}
	
	void OnDisable()
	{
		GUIManager.OnGUIStateChanged -= OnGUIStateChanged;
	}
	
	void OnGUIStateChanged (GUIState guiState)
	{
		switch (guiState) {
			
		case GUIState.Options:
			panel.IsVisible = true;
			break;
			
		default:
			panel.IsVisible = false;
			break;
		}
	}

	public void Back()
	{
		GUIManager.GUIState = GUIState.MainMenu;
	}
}
