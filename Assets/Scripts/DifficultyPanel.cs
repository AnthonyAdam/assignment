using UnityEngine;
using System.Collections;

public class DifficultyPanel : MonoBehaviour {

	private dfPanel panel;
	
	void OnEnable()
	{
		panel = GetComponent<dfPanel> ();
		GUIManager.OnGUIStateChanged += OnGUIStateChanged;
	}
	
	void OnDisable()
	{
		GUIManager.OnGUIStateChanged -= OnGUIStateChanged;
	}
	
	void OnGUIStateChanged (GUIState guiState)
	{
		switch (guiState) 
		{
		default:
			panel.IsVisible = false;
			break;

		case GUIState.Difficulty:
			panel.IsVisible = true;
			break;
		}
	}
	public void Back()
	{
		GUIManager.GUIState = GUIState.MainMenu;
	}
}
